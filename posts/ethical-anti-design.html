<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>njms - Ethical anti-design, or designing products that people can't get addicted to.</title>

        <link rel="stylesheet" type="text/css" href="/assets/css/styles.css">

        <meta property="og:title" content="Ethical anti-design, or designing products that people can't get addicted to."/>
<meta property="og:description" content="Ethical anti-design is a design practice used in Resin where you intentionally add friction (or at least, what might feel like friction) to an app's interface in order to prevent addictive or otherwise unhealthy behavior."/>
<meta property="og:type" content="article"/>
<meta property="og:url" content="https://njms.ca/posts/ethical-anti-design.html"/>



  <meta property="og:image" content="https://njms.ca/assets/img/post-thumbnails/ethical-anti-design.png"/>


    </head>
    <body>
        <div class="sidebar">
    <nav>
      <p class="sidebar-header">
        <a href="/">
          njms.ca/
        </a>
      </p>
        <ul class="links">
            <li><a href="/wiki/">wiki/</a></li>
<li><a href="/about.html">about.html</a></li>
<li><a href="/blog.html">blog.html</a></li>
<li><a href="/links.html">links.html</a></li>
<li><a href="/now.html">now.html</a></li>

        </ul>
        <ul class="sidebar-posts">
            
            
                
                <li class="sidebar-post">
                    <a href="/posts/leap-of-faith-vs-willingness-to-know.html">
                        A leap of faith versus the willingness to Know
                    </a>
                    <br />
                    2023-12-29
                </li>
                
            
                
                <li class="sidebar-post">
                    <a href="/posts/my-principles-are-your-technical-debt.html">
                        My principles are your technical debt
                    </a>
                    <br />
                    2023-12-26
                </li>
                
            
                
                <li class="sidebar-post">
                    <a href="/posts/everything-is-a-stream.html">
                        Everything is a Stream
                    </a>
                    <br />
                    2023-12-13
                </li>
                
            
                
                    
        </ul>
    </nav>
</div>

        <div class="main-container">
            <div class="top-navbar">
    <p class="top-nav-header"><a href="/">njms.ca</a></p>
    <nav>
        <ul class="vertical-list">
          <li><a href="/wiki/">wiki/</a></li>
<li><a href="/about.html">about.html</a></li>
<li><a href="/blog.html">blog.html</a></li>
<li><a href="/links.html">links.html</a></li>
<li><a href="/now.html">now.html</a></li>

        </ul>
    </nav>
</div>

            <main>
                <div class="post">
                    <h1 class="post-title">Ethical anti-design, or designing products that people can't get addicted to.</h1>
                    <p class="date">Published on 2021-02-21</p>
                    <hr>
                    
                        <figure>
                            <img src="/assets/img/post-thumbnails/ethical-anti-design.png" alt="A Wiimote sitting on a table next to an open window, as displayed on old Wii games">
                            <figcaption>
                                Why not take a break?
                            </figcaption>
                        </figure>
                    

                    <p>In the photo above, a Wii Remote is sitting on a table next to an open window. People who grew up playing the Wii might remember it; while playing Wii Sports, the game would give you a pop up window of it with a message politely reminding you that you can always take a break. While it might seem a bit counterintuitive, there's a couple of different reasons why a game would tell you to stop playing. In the case of online, subscription-based games, the company might actually benefit from you logging off every once in a while (don't worry, I'll get to social media eventually). It's not like you're cancelling your subscription, and every second you're online chews up valuable bandwidth. In other games, things like level-grinding, while tempting, might ruin the experience. In a game like Pokémon, for example, if you get your entire team up to level 60 before taking on the Elite Four, you'll win, but not in the most glamorous or satisfying way. The case of the Wii was a bit different. It seemed like Nintendo was aware that their audience was mostly younger children, and they felt the need to intervene when the player spends too much time playing the game out of interest for the player themself (and maybe the appeasement of the parents). That's not the sort of thing we see often anymore, and I always wondered why.<span data-separator></span></p>
<p>Bandwidth isn't as expensive as it used to be. The mechanics of many modern video games don't allow for level grinding anymore. Has Nintendo stopped caring about our children? Well, the industry has changed quite a bit. Today, many games benefit from having players spend as much time online as possible. In the mobile game Clash of Clans, players can prevent their base from being attacked by always staying online. The faster you complete a game, the quicker you'll be pushed to buy its DLC or better yet, its Season Pass. Most obviously, if you spend more time online, you'll encounter more players, some with creative (and expensive) skins or other purchasable cosmetic items. The more people like this you see, the more you may feel pressured into buying them yourself. This is a worrying trend, and it seems to be affecting most of the Triple-A game market. However, we have someone else to thank for these company's drive to keep our eyes on our devices for as long as possible.</p>
<p>In the case of social media, it was always in their best interest to keep us online. Very simply, the longer you look at your screen, the more likely your eyes are to come across an advertisement. Social media companies have poured billions of dollars into refining techniques to keep us engaged. I often joke that TikTok is the parent function of this sort of thing: short, colourful, algorithm-tailored, multi-sensory content that's constantly being streamed to you without interruption. The only thing that could make it more addictive would be if they IV-ed sugar directly into your veins while they were at it. And still, TikTok probably invests billions of dollars every year in refining their platform to make it as inviting as possible. Instagram is following suit; the way they present content is starting to feel more and more like TikTok with each passing month (especially since they introduced Reels). It's no surprise my generation spends so much time on social media, and nobody can blame them. As I was researching this topic, taking a closer look at the user interface of Instagram, my biggest challenge was to hold myself back from just scrolling through the endless stream of algorithmically chosen content.</p>
<p>It's scary to think about. So, I'll ask. All this user-retention stuff is great, but what if it didn't have to be like that? Rather, what if we designed products that weren't designed to be addictive?</p>
<h2>Dark Patterns and the Fediverse</h2>
<p>I get it, a big part of an app's addictiveness comes from its usability, and the notion of designing apps to be less usable feels a bit ridiculous. That being said, there might be a market for it. Last week, I wrote an article called &quot;<a href="the-fediverse-solves-half-the-problem.html">The Fediverse only solves half the problem</a>&quot; where I talked about the issue of dark patterns such as infinite scrolling at length. The point is, FLOSS-abiding technology has a philosophical obligation to put the people using it first. In the case of social media, a big part of that was creating an ecosystem (i.e. the Fediverse) where you didn't need to depend on a central authority; an ecosystem where you could be in charge of your own data. While the organization of the Fediverse was a big step forward, we still have to deal with dark patterns.</p>
<p>Most open-source developers (myself included) don't have much formal work experience in UI/UX design. That's why so much open-source software is either extremely brutalist (see Blender) or very, very closely related to the design of its proprietary counterpart (see KDE). The Fediverse seems to fall into the second category.The UI of Mastodon (and all of its clients) looks quite a bit like Twitter. In a sense, this was probably intentional. The closer the UI designers of Mastodon could mimic Twitter, the more newcomers would immediately feel comfortable with the Fediverse. With that comes the problem of dark patterns. Again, Twitter's interface was very intentionally designed to maximize the amount of time per day a person spends online. The Fediverse really doesn't need that, but it has it anyway. That's why I'm proposing that we reevaluate the way we design Fediverse interfaces and clients in light of how we can best strike a balance between creating a positive experience and one that puts the well-being of the person using your product first.</p>
<h2>What is ethical anti-design?</h2>
<p>Ethical anti-design comes in two parts. First of all, it's worth asking more generally, <em>what exactly is anti-design?</em></p>
<p><img src="/assets/img/post-figures/danger-zone.png" alt="Github's &quot;danger zone&quot; where all the dangerous repository actions happen" /></p>
<p>Aesthetically, anti-design was an Italian design movement that served as a critique on consumer culture<sup class="footnote-reference"><a href="#1">1</a></sup>. While spiritually relevant, that's not the kind of anti-design I'm talking about. Instead, I'm talking about the much more pragmatic anti-design that's really common in interfaces. Anti-design is kind of like the opposite of design: designing not for usability, but for unusability. Again, somewhat unintuitive, but it can be very useful. We've all encountered anti-design many times in our lives. You know when the trash can on your computer asks you to confirm before permanently deleting your files? That's anti-design. Dangerous actions like permanently deleting files are often necessary. That being said, the designers want to make sure you don't do it by accident. That's why they implement things like confirmation checks to make sure that doesn't happen.</p>
<p>One of my favourite examples is how Github deals with repository deletion. On the settings page of your Github repository, all the &quot;dangerous&quot; actions are grouped together in a very scary looking red box. At the very bottom of that box is the option to permanently delete the repository. If you click on it, Github takes this shtick one step further and asks you to type the name of the repository into a dialogue box. This forces you to be fully, consciously aware of what you're doing as you're doing it, and rightfully so. Deleting the wrong repository could be frustrating, and if you're dealing with coordinating a large number of collaborators, devastating. Fortunately, it's not every day that you have to delete a Github repository so the company is fairly safe in making it a reasonably difficult thing to do.</p>
<p>Ethical anti-design takes these ideas and applies them to an ethical code.</p>
<p>If we were all Marcus Aurelius then maybe we wouldn't need to worry about social media addiction. The thing is, not everybody is a pure being of stoic efficiency; most people respond really well (or rather, really poorly) to behavioral design. Anti-design puts interventions in place to ensure a person doesn't make dangerous actions unintentionally. Ethical anti-design asks, &quot;what else might be considered dangerous, as per my ethical code?&quot; As I've surely made clear by now, one action that I consider dangerous is infinite scrolling. So, if I'm designing an app and I want to apply ethical anti-design, then I need to make sure the right measures are in place to prevent the person from zoning out and scrolling through social media all day.</p>
<h2>What does ethical anti-design look like?</h2>
<p>In the case of infinite scrolling, I think there's two routes an ethical anti-designer could take to help the person using their product scroll healthily: they could get rid of it or they could interrupt it.</p>
<p>Before we had infinite scrolling, we had pagination. In fact, pagination is still relatively common. Content is broken down into separate pages. When the person scrolls to the bottom of the page, if they want to keep looking at content, they have to click the &quot;next&quot; button and wait for the new content to load. This feels a bit antiquated now that we have effective asynchronous web applications, but there are two key features here. First of all, the time it takes for the website to load in the new content interrupts the otherwise seamless scrolling. This period of time—being either an instant or several seconds—forces the person to pause and maybe even think about what they are doing. If they have a moment to reflect on it, then maybe they'll know for themself whether or not this is what they ought to be doing. The other component of this is the &quot;next&quot; button. This button tells the person &quot;that's all, though there is more if you want it.&quot; This forces the user to actually think about what they're doing, which helps to prevent mindless scrolling as well.</p>
<p>The other option would be simply to interrupt the infinite scrolling. This could be very similar to pagination, or it could be very different. Regardless, the way you go about doing it is very similar: force the user to actively choose whether or not they want to see more content and offer them an opportunity to take a pause. Imagine this: you're scrolling through Instagram posts. After about twenty posts, you encounter a button that says &quot;Show More.&quot; To see more content, you need to hold down the button for a little more than one second. As you do this, you're shown an animated circle that completes once the time is up. Once that happens, twenty more posts are shown. This ensures that you aren't relying on the person's network connection to break up their scrolling, giving just enough time to make the user completely aware that this experience isn't continuous. The button, again, forces the user to physically interact with the interface to see more content. In practice, this would be very similar to pagination, but it would feel much more natural to someone who is used to infinite scrolling being the norm.</p>
<p>However, infinite scrolling isn't the only thing that causes social media addiction. Another way they hook us is by getting us into the numbers game. In my generation, a lot of one's self-esteem comes from your followers, your follower to following ratio and how many likes and comments you get on your posts. In reality, I haven't encountered many people who are seriously judgmental about this sort of thing; most people are harder on themselves about it than anyone else ever could be. Interestingly enough, this is something that Instagram actually seems to be aware of. They've redesigned the profile page a number of times to change the emphasis on people's follower count, and more recently, they've updated the look of posts to hide the number of likes they've received. It's still technically possible to see how many likes someone else's post has received, but you have to jump through enough hoops to make it not incredibly worthwhile. I would also consider this ethical anti-design: people are tempted to compare themselves to others online, and Instagram is intervening. Personally, I think this could go much further. For an example, Instagram already shows mutual followers on a person's profile page; what if they didn't show your follower count at all? If we were to use social media as a way to connect to people we know, then your follower count shouldn't really matter, should it?</p>
<h2>Where's all this going?</h2>
<p>Behavioral design is as problematic as it's standard practice among the social media giants. Naturally, these dark patterns have leaked into the Fediverse. The only way for us to to escape these patterns is to make an active effort to avoid them. It won't be easy; I too have a hard time bringing myself to use websites that still use rudimentary pagination, but at the same time, that's kind of the point.</p>
<p>If I haven't already made it super obvious, I intend on developing a new Fediverse client that applies ethical anti-design. My plan will probably take this idea to its extremes. The goal here isn't necessarily to create the Fediverse client everyone will use, but rather to demonstrate what this sort of thing could look like. My hope is that ultimately, other developers will be inspired by it and try to work these ideas into their own projects. I don't have a lot of information on it right now because frankly I'm still very early in the process, but if you want to hear more about it I'd encourage you to <a href="https://social.coop/@njms">follow me on the Fediverse</a> where I plan on talking about this project quite a bit and right here on this blog where you can expect to see a more official announcement in the coming weeks.</p>
<p>If this is something you'd be interested in working on or in any way helping out with, I'd love to hear from you!</p>
<h2>Footnotes</h2>
<div class="footnote-definition" id="1"><sup class="footnote-definition-label">1</sup>
<p>Oxford Reference, &quot;Anti Design.&quot; [Online]. Available: <a href="https://www.oxfordreference.com/view/10.1093/oi/authority.20110803095416737" aria-label="Link to reference 1">https://www.oxfordreference.com/view/10.1093/oi/authority.20110803095416737</a>.</p>
</div>


                    <h2> Comment on this article</h2>
                    If you want to share your thoughts on this article,
                    <a href="mailto:doreply@njms.ca?subject=Reply to 'Ethical anti-design, or designing products that people can't get addicted to.'&body=Preferred%20name%3A%20%5Byour%20name%5D%0AWeb%20presence%3A%20%5Bwebsite%2C%20social%20media%20profile%2C%20both%2C%20etc.%5D%0ADo%20you%20want%20this%20published%20on%20njms.ca%3F%20%5Byes%2Fno%2C%20no%20by%20default%5D%0A%0AType%20your%20response%20here...">
                      click here to compose and send your response via email
                    </a>.
                    If you want your comment published, please say so and know
                    it'll be reviewed beforehand. If you say something mean, you
                    may successfully hurt my feelings.
                </div>
            </main>
        <footer>
    <p class="dinkus" aria-hidden="true">* * *</p> 

			<p>
				Hand-crafted on the unceeded land of the sovereign
				<a href="https://www.syilx.org/">Syilx Okanagan Nation</a>.
			</p>

    <p class="license">
        Unless otherwise noted, content is shared under the
        <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/"
            rel="license">
          CC-BY-NC-SA 4.0
        </a>.<br>

        All code is shared under the
        <a href="https://www.gnu.org/licenses/agpl-3.0.en.html"
            rel="license">
            AGPL
        </a> (<a href="https://codeberg.org/njms/pages">source</a>).<br>
        Built with
        <a href="https://cobalt-org.github.io/">
          Cobalt.rs
        </a>
    </p>

    <ul class="vertical-list">
      <li>
    <a href="https://social.coop/@njms" rel="me">fediverse</a>
</li>
<li>
    <a href="https://codeberg.org/njms">codeberg</a>
</li>
<li>
    <a href="https://keyoxide.org/hkp/382B52FBB011915692F8D878B53AB05285D710D6">
      keyoxide
    </a>
</li>

    </ul>
    <ul class="vertical-list">
        <li><a href="/rss.xml">rss</a></li>
        <li><a href="/feed.json">json</a></li>
        <li>
          <a href="/njms.asc">
            pgp
          </a>
        </li>
    </ul>
    <div class="webrings">
      <p>
        <a href="https://fediring.net/previous?host=njms.ca">←</a>
        <a href="https://fediring.net/">Fediring</a>
        <a href="https://fediring.net/next?host=njms.ca">→</a>
      </p>
      <p>
        <a href="https://hotlinewebring.club/njms/previous">←</a>
        <a href="https://hotlinewebring.club/">Hotline Webring</a>
        <a href="https://hotlinewebring.club/njms/next">→</a>
      </p>
      <!-- Need to apply before I can meaningfully display this
      </p class="webring">
      <p>
        <a href="http://geekring.net/site/NUMBER/previous">←</a>
        <a href="http://geekring.net/">Geekring</a>
        <a href="http://geekring.net/site/NUMBER/next">→</a>
      </p>
      -->
    </div>

    <p>Last built 2024-02-07 </p>

</footer>

    </body>
</html>
