<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>njms - Monads are scary; or, The Queering of Object Orientated Programming</title>

        <link rel="stylesheet" type="text/css" href="/assets/css/styles.css">

        <meta property="og:title" content="Monads are scary; or, The Queering of Object Orientated Programming"/>
<meta property="og:description" content="Thoughts on the naturalization of object-orientated programming and escaping it."/>
<meta property="og:type" content="article"/>
<meta property="og:url" content="https://njms.ca/posts/monads-are-scary.html"/>



  <meta property="og:image" content="https://njms.ca/assets/img/post-thumbnails/monads-are-scary.jpg"/>


    </head>
    <body>
        <div class="sidebar">
    <nav>
      <p class="sidebar-header">
        <a href="/">
          njms.ca/
        </a>
      </p>
        <ul class="links">
            <li><a href="/wiki/">wiki/</a></li>
<li><a href="/about.html">about.html</a></li>
<li><a href="/blog.html">blog.html</a></li>
<li><a href="/links.html">links.html</a></li>
<li><a href="/now.html">now.html</a></li>

        </ul>
        <ul class="sidebar-posts">
            
            
                
                <li class="sidebar-post">
                    <a href="/posts/leap-of-faith-vs-willingness-to-know.html">
                        A leap of faith versus the willingness to Know
                    </a>
                    <br />
                    2023-12-29
                </li>
                
            
                
                <li class="sidebar-post">
                    <a href="/posts/my-principles-are-your-technical-debt.html">
                        My principles are your technical debt
                    </a>
                    <br />
                    2023-12-26
                </li>
                
            
                
                <li class="sidebar-post">
                    <a href="/posts/everything-is-a-stream.html">
                        Everything is a Stream
                    </a>
                    <br />
                    2023-12-13
                </li>
                
            
                
                    
        </ul>
    </nav>
</div>

        <div class="main-container">
            <div class="top-navbar">
    <p class="top-nav-header"><a href="/">njms.ca</a></p>
    <nav>
        <ul class="vertical-list">
          <li><a href="/wiki/">wiki/</a></li>
<li><a href="/about.html">about.html</a></li>
<li><a href="/blog.html">blog.html</a></li>
<li><a href="/links.html">links.html</a></li>
<li><a href="/now.html">now.html</a></li>

        </ul>
    </nav>
</div>

            <main>
                <div class="post">
                    <h1 class="post-title">Monads are scary; or, The Queering of Object Orientated Programming</h1>
                    <p class="date">Published on 2022-04-25</p>
                    <hr>
                    
                        <figure>
                            <img src="/assets/img/post-thumbnails/monads-are-scary.jpg" alt="An abstract image of light shining through a passageway across patterned surfaces">
                            <figcaption>
                                The light shining down on all those who accept OOP as their organizing ideology. This picture honestly bares zero relation to the article but every article on my blog needs to have a photo and I haven't gotten around to fixing that. Photo by <a href="https://unsplash.com/@panevesht">Payam Moin Afshari</a> on Unsplash.
                            </figcaption>
                        </figure>
                    

                    <p>I've been calling myself a functional programmer for a number of years, and I've worked on a number of projects in primarily functional programming languages. But still, there was always one thing that I could never figure out: monads. Today, that changed.</p>
<h2>What is a monad?</h2>
<blockquote>
<p>In functional programming, a monad is a software design pattern with a structure that combines program fragments (functions) and wraps their return values in a type with additional computation. (<a href="https://en.wikipedia.org/wiki/Monad_(functional_programming)" aria-label="Source for the quote defining monads on Wikipedia">Wikipedia</a>)</p>
</blockquote>
<p>That's it.</p>
<p>While I was mindlessly scrolling through Wikipedia articles waiting for my computer science exam to start, I saw one article that linked to another called &quot;Railway orientated programming,&quot; which I had never heard of. So, I clicked on it, and it redirected me to Wikipedia's page for monads. <em>Gah!</em> I thought. But then, the first line caught my eye, and it clicked.</p>
<p>Now, I say &quot;that's it&quot; as though I assume it'll be obvious to everyone, and it's certainly not. This is particularly the case if you haven't done much FP in the past. For me, it was the realization that I had been using monads all the time without even noticing. So, if that sentence doesn't make much sense, I'll walk through the example that same Wikipedia article gives. If you already know what monads are, you're not going to learn anything new by reading the next section, so feel free to skip it. Especially if you know enough about monads to poke holes in my explanation. We wouldn't want that to happen /s</p>
<h3>Avoiding null pointer exceptions with <code>Maybe</code></h3>
<p>Having to code around null values is a pain, and they come up everywhere. If you're not constantly checking to see if a variable is null, then there's always that haunting chance that it'll show up where you least expect it. This can be a big problem in large code bases.</p>
<p>Many functional programming languages railroad you into making these checks by swapping out the classic &quot;null&quot; for a type similar to Haskell's <code>Maybe</code>. <code>Maybe</code> is a union type, and if your language doesn't have those, then all you need to know is that union types can be one of a finite set of things. Booleans are union types, and in some languages, that's how they're defined: either <code>true</code> or <code>false</code>.</p>
<p>Each &quot;part&quot; of a union type can also contain some value to go with it. That's what <code>Maybe</code> does. <code>Maybe</code>, like the Boolean, has two options: <code>Just T</code> or <code>Nothing</code>, where <code>T</code> is some other type. Thus, if your function returns a <code>Maybe</code> and you get <code>Nothing</code>, then that means there's no result. Otherwise, you get <code>Just T</code>, and if you properly unwrap it, you'll get the value you're looking for. This is that &quot;extra computation&quot; the Wikipedia definition was talking about.</p>
<p>So that's an example of a monadic type. Monadic types are just one part of the larger pattern. A monad also needs two functions: one that wraps a type in the monadic type (say, taking <code>t</code> and turning it into <code>Just t</code>) and another that takes a value wrapped in a monadic type and turns it into another value wrapped in a monadic type. You can think of that as two functions &quot;passing&quot; the value between each other, without having to go through the process of unwrapping the <code>Maybe</code> every time. With those three simple things, you can accomplish quite a bit that would otherwise seem impossible in functional programming.</p>
<p>That's a pretty high level explanation, but rigorously defining monads is not exactly the point of this article. For a better definition, try reading <a href="https://stackoverflow.com/questions/44965/what-is-a-monad" aria-label="First Stack Overflow question explaining monads">these</a> <a href="https://stackoverflow.com/questions/7975022/haskell-monad-bind-operator-confusion" aria-label="Second Stack Overflow question explaining monads">two</a> questions on Stack Overflow.</p>
<h2>Why is it so hard?</h2>
<p>I've spent most of my life thinking that monads were an extremely abstract mathematical concept that just kind of happened to sneak into programming languages, and that they were only supposed to be understood by grad students doing their Master's thesis on category theory. I've also spent my entire life being told by functional programmers that monads <em>aren't</em> some abstract, esoteric concept, and that they were pretty intuitive given the right framework. Of course, those functional programmers were never able to transfer that framework to me. Whenever they'd try, my brain would just shut them out.</p>
<p>I never really thought of myself as an &quot;object-orientated programmer&quot; (at least, not until I started having to fill out job applications that insisted on applicants being well-versed in OOP, for whatever reason). To borrow a concept from gender studies, &quot;object-orientated&quot; is a sort of hidden adjective when talking about programming. Hidden adjectives are the adjectives we convey implicitly--not because they are necessarily true, but rather because they're understood to be the norm<sup class="footnote-reference"><a href="#1">1</a></sup>. Not every programmer is necessarily an object-orientated programmer, but it feels kind of weird to make that explicit considering how common object-orientated programming is. Notably, it doesn't feel nearly as weird to call someone a &quot;functional programmer;&quot; that's less obvious and as such needs to be marked.</p>
<p>There's a certain ideology present among a lot of computer science educators that OOP is the natural way to write code. There's nothing natural about this, and in fact, not too long ago, OOP didn't even exist. Object-orientated programming certainly feels pretty natural when you're introduced to it through the textbook examples that ensure that every object pairs perfectly with some real-world, unabstracted &quot;thing,&quot; like <code>Cat</code> or <code>BlueCar</code>. However, those models poorly represent the kind of abstractions you need to make when programming.</p>
<p>I can remember sitting in my first-year computer science class where, for the first time, I got the actual, textbook definitions of polymorphism, inheritance and encapsulation. Seeing those examples we covered in class worked their magic on me as well, and coming out of it, I was feeling a lot more inclined to lean on classes in my own projects. However, I've also seen the nightmare that your code base can turn into when you're not careful with it. And it's kind of hard to talk about that with people who've been freshly indoctrinated into object-orientated ideology--at least, not without being a weirdo.</p>
<h3>Side note on &quot;getting good&quot;</h3>
<p>There is a reasonable argument that could be made by senior software developers who work primarily in OOP that I'm just bad at it and don't know what I'm talking about. I agree with both of those conclusions (without being too hard on myself). There are libraries worth of books written on how to do OOP correctly that promise, should you follow in their footsteps, that you can avoid all of those &quot;nightmares&quot; OOP &quot;causes.&quot;</p>
<p>I do, however, want to note that what I'm talking about here has more to do with creating a system in which newer programmers can do more good things quicker. The same kind of argument can be applied to the case of C++ versus Rust. Sure, you could just get good at memory management and accomplish everything that Rust can do with a much more mature technology. Or, you could just let everything explode and patiently walk through your problems with the compiler. The latter (reasonably) guarantees that things won't go wrong.</p>
<h3>A very productive conversation</h3>
<p>I do have one friend though, going to a different university, who was very interested in the concept. She was taking a very different first-year computer science course that taught Scheme and C instead of Java. So, her first exposure to programming was functional. As someone who was interested in continuing with the program, this person was interested in what to expect from the upper-year computer science courses that focused on OOP.</p>
<p>But, how do you explain OOP to someone who's only ever used functional and procedural programming languages?</p>
<blockquote>
<p><em>What is object-orientated programming?</em></p>
</blockquote>
<p>It's a different way of framing your code. Instead of doing everything with functions, you're trying to do everything with classes.</p>
<blockquote>
<p><em>What's a class?</em></p>
</blockquote>
<p>A class is kind of like a container for data.</p>
<blockquote>
<p><em>So like a list?</em></p>
</blockquote>
<p>Kind of, but things aren't indexed by numbers. Every element has a name.</p>
<blockquote>
<p><em>So like a struct?</em></p>
</blockquote>
<p>Not exactly. The thing that makes classes unique is that they have methods that are bound to the data.</p>
<blockquote>
<p><em>What's a method?</em></p>
</blockquote>
<p>Well, a method is kind of like a function.</p>
<blockquote>
<p><em>What's the difference?</em></p>
</blockquote>
<p>A method can only act on the instance of the class.</p>
<blockquote>
<p><em>So it's like a less useful function.</em></p>
</blockquote>
<p>Well, it can be just as useful if your functions are polymorphic.</p>
<blockquote>
<p><em>What's that?</em></p>
</blockquote>
<p>A polymorphic function is a function that can have more than one type of value passed to it.</p>
<blockquote>
<p><em>So like a generic function?</em></p>
</blockquote>
<p>Ugh.</p>
<p>If you keep trying to explain this, as I have, and you're about as biased towards functional programming as I am, you'll start to feel like you're reinventing everything that can be done with procedural or functional code in a less obvious and more complicated way.</p>
<p>Now, the takeaway from this isn't that functional programming is somehow superior to object orientated programming. In my experience, it feels a lot more like a matter of preference. It's just interesting to consider how our &quot;upbringing&quot; in programming shapes the way we think about everything we subsequently encounter. When you're trying to transition to a primarily functional programming language, it's really hard to unlearn all of the things that have been drilled into your mind by your CS professors and/or boot camp instructors. The biggest thing that turned me away from Haskell when I first approached it was that I simply couldn't imagine how I would create any program more complicated than a calculator with it. It just seemed unimaginable to me.</p>
<p>The same thing goes for everyone who was first introduced to programming in my friend's Scheme class.</p>
<p>Technically, the class I took in my first year first introduced us to procedural programming before moving into object orientated programming in the second semester. I still take issue with the fact that OOP was treated as somehow being the natural evolution from procedural, which, as far as I'm concerned, is completely untrue. Maybe I'm just miffed that my school doesn't offer any courses that cover functional programming. I don't even take issue with the fact that object orientated programming is so common in universities. This makes a lot of sense considering how popular OOP is in the industry. How could they not teach it? What matters to me is how swift people are to naturalize it. Instead, if educators focused on introducing both OOP and FP, students would come away with two unique tool sets to solve unique problems in elegant ways.</p>
<p>Knowing more than one way to solve a problem goes a long ways towards enabling you to synthesize your own solutions to new problems that you never could have anticipated. That's a pretty general framework for solving problems in any discipline. To do that, one must grow out of the frameworks they've always thought natural.</p>
<h2>See also</h2>
<ul>
<li><a href="https://yewtu.be/watch?v=QM1iUe6IofM&amp;t=947s">Object-Orientated Programming is Bad</a> by Brian Will. This video inspired some of the ideas from this post.
<ul>
<li>If the instance is down, copy this to your preferred Invidious instance: <code>watch?v=QM1iUe6IofM&amp;t=947s</code></li>
</ul>
</li>
</ul>
<h2>Footnotes</h2>
<div class="footnote-definition" id="1"><sup class="footnote-definition-label">1</sup>
<p>A. Braithwaite and C. M. Orr, “Knowledges,” in <em>Everyday Women's and Gender Studies</em>, 1s ed., New York: Routledge, Taylor &amp; Francis Group, 2017.</p>
</div>


                    <h2> Comment on this article</h2>
                    If you want to share your thoughts on this article,
                    <a href="mailto:doreply@njms.ca?subject=Reply to 'Monads are scary; or, The Queering of Object Orientated Programming'&body=Preferred%20name%3A%20%5Byour%20name%5D%0AWeb%20presence%3A%20%5Bwebsite%2C%20social%20media%20profile%2C%20both%2C%20etc.%5D%0ADo%20you%20want%20this%20published%20on%20njms.ca%3F%20%5Byes%2Fno%2C%20no%20by%20default%5D%0A%0AType%20your%20response%20here...">
                      click here to compose and send your response via email
                    </a>.
                    If you want your comment published, please say so and know
                    it'll be reviewed beforehand. If you say something mean, you
                    may successfully hurt my feelings.
                </div>
            </main>
        <footer>
    <p class="dinkus" aria-hidden="true">* * *</p> 

			<p>
				Hand-crafted on the unceeded land of the sovereign
				<a href="https://www.syilx.org/">Syilx Okanagan Nation</a>.
			</p>

    <p class="license">
        Unless otherwise noted, content is shared under the
        <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/"
            rel="license">
          CC-BY-NC-SA 4.0
        </a>.<br>

        All code is shared under the
        <a href="https://www.gnu.org/licenses/agpl-3.0.en.html"
            rel="license">
            AGPL
        </a> (<a href="https://codeberg.org/njms/pages">source</a>).<br>
        Built with
        <a href="https://cobalt-org.github.io/">
          Cobalt.rs
        </a>
    </p>

    <ul class="vertical-list">
      <li>
    <a href="https://social.coop/@njms" rel="me">fediverse</a>
</li>
<li>
    <a href="https://codeberg.org/njms">codeberg</a>
</li>
<li>
    <a href="https://keyoxide.org/hkp/382B52FBB011915692F8D878B53AB05285D710D6">
      keyoxide
    </a>
</li>

    </ul>
    <ul class="vertical-list">
        <li><a href="/rss.xml">rss</a></li>
        <li><a href="/feed.json">json</a></li>
        <li>
          <a href="/njms.asc">
            pgp
          </a>
        </li>
    </ul>
    <div class="webrings">
      <p>
        <a href="https://fediring.net/previous?host=njms.ca">←</a>
        <a href="https://fediring.net/">Fediring</a>
        <a href="https://fediring.net/next?host=njms.ca">→</a>
      </p>
      <p>
        <a href="https://hotlinewebring.club/njms/previous">←</a>
        <a href="https://hotlinewebring.club/">Hotline Webring</a>
        <a href="https://hotlinewebring.club/njms/next">→</a>
      </p>
      <!-- Need to apply before I can meaningfully display this
      </p class="webring">
      <p>
        <a href="http://geekring.net/site/NUMBER/previous">←</a>
        <a href="http://geekring.net/">Geekring</a>
        <a href="http://geekring.net/site/NUMBER/next">→</a>
      </p>
      -->
    </div>

    <p>Last built 2024-02-07 </p>

</footer>

    </body>
</html>
