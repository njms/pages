<!DOCTYPE html>

<html lang="en">
  <head>
    <title>njms - wiki</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../assets/css/wiki.css">
  </head>
  <body>
    <div class="main-container">
      <main>
        <p>
          <a href="/">&lt;- leave the wiki</a> or <a href="index.html">go home</a>
        </p>

        <h1>A flexible and humane strategy for weekly planning</h1>
<p>Tags: <a href='living.html'>living</a></p>
<p>The Post-Effective Priority Queue is the name I came up with on the spot for a system I've been using for around two years now to schedule things I need to do over the course of a week. &quot;Post-effective&quot; implies here that, to a certain extent, it comes from Stephen Covey's ideology of &quot;effectiveness&quot; but tries to learn from its mistakes and move past them in a meaningful way.</p>
<h2>Motivation</h2>
<h3>Todo lists</h3>
<p>Todo lists are a very common tool to schedule tasks. All you have to do is break all your responsibilities into discrete tasks and cross them off your list as you complete them. You can order them by priority and/or complete them based on how much energy you have at a given moment.</p>
<p>Probably the biggest issue with todo lists is that they poorly represent how tasks should be distributed over time; instead, they tend to take on the form of a single fire hose of work. This is appropriate for some, but not all responsibilities.</p>
<h3>Time-blocking</h3>
<p>Time-blocking is when you designate &quot;blocks&quot;, or whole periods of time to specific tasks. This can be done in conjunction with daily/weekly planning, though it would probably be unfeasible in monthly planning.</p>
<p>This is, in my opinion, the optimal scheduling strategy if you're optimizing for tasks completed and minimal time wasted. Realistically, though, this strategy has a number of problems:</p>
<ul>
<li>It's hard to estimate how long a task will take. You will regularly overwork yourself to complete things by your self-imposed deadlines or find you have nothing else to do.</li>
<li>Time-blocking assumes you can stay focused on a single task for a given period of time. Some can achieve this, but not everyone</li>
<li>Time-blocking is inflexible, and missing deadlines can result in needing to do lots of rescheduling on the fly</li>
</ul>
<p>However, time-blocking can help to contextualize tasks in a larger daily or weekly plan, which can be helpful with motivation.</p>
<h3>Planning periods</h3>
<p>Daily planning is ineffective for solving long-term problems. Monthly planning demands so much flexibility that most of your time planning things in advance will be wasted.</p>
<p>Certain self-proclaimed self-help gurus will tell you that weekly planning is a good compromise between these two extremes. I have found this to be mostly true in my personal experience. Some issues with it, however, include:</p>
<ul>
<li>You can't predict the future and will inevitably need to do lots of rescheduling</li>
<li>Focusing only on weeks can lead you to lose sight of the bigger picture</li>
</ul>
<h2>The Post-Effective Priority Queue</h2>
<p>The Post-Effective Priority Queue (PEPQ) is a set of three priority queues that describes all the discrete tasks you want to accomplish over the course of a day. The first is designated for the morning, the second for the afternoon, and the third for the evening.</p>
<p>Each day is given its own PEPQ. At the beginning of the week, you list out everything you need to get done by considering where you are with your projects and revisiting deadlines, and then you assign each task to one of the three priority queues. How exactly you assign them is very personal, and it can take a while to figure out what works best for you.</p>
<p>When you start your day and you're ready to do work, you start with the top entry in the morning PQ, and move on until it's empty. Between completing each PQ, you should probably include some longer break, like eating lunch or supper.</p>
<p>If you don't complete all the items in your priority queue before you move on to the next unit of time, then you can quickly decide to reschedule them to tomorrow, or have all of the tasks roll over into the next PQ, depending on what makes more sense for the given tasks.</p>
<p>If several PQs start to pile up with unfinished work, then you need to take a step back and seriously reconsider how you're trying to schedule your work. It's possible you're either being overworked, or that you need to re-evaluate how you're prioritizing tasks.</p>
<h3>Aside: wait, what's a priority queue exactly?</h3>
<p>A priority queue is a data structure used quite a bit in software engineering. It's an ordered list of things, where their order is determined by some kind of &quot;priority.&quot; When new items are put into the priority queue, they are inserted wherever their priority dictates they be. So, if you have three low-priority items in your priority queue and you add a high-priority item, then the new one is going to be next up in line. This is exactly how hospital emergency rooms order patients. In Canada, at least. Elsewhere, I get the feeling it has more to do with how much money you have.</p>
<p>In this context, we might choose to think of the priority as &quot;how urgent they are,&quot; but in practice, what this priority actually represents is a lot more subtle and determined through lots of trial and error. Very, very generally, I think I might say that I usually order tasks by some combination of what kind of work they represent, how it'll make me feel to complete those tasks, and how well they would match how much energy I expect to have at a given point in the day.</p>
<h3>How does the PEPQ compare to other strategies?</h3>
<p>Here's how I believe the PEPQ improves upon or similarly fails like the other strategies listed above:</p>
<ul>
<li>The PEPQ is an improvement over todo lists because it captures the same freedom in a more time-aware manner</li>
<li>The PEPQ is an improvement over time blocks because it maintains its time-awareness without sacrificing flexibility. It accomplishes this by assigning tasks to longer, abstract periods of time like morning, afternoon and evening</li>
<li>Further, the PEPQ system assumes that you won't be able to do everything in time and implements a standard procedure for rescheduling work automatically in a way that doesn't over-burden you. That being said, it's still possible for uncompleted tasks to pile up.</li>
<li>The PEPQ system invites you to spend a longer period of time at the beginning of the week to figure out the details and make smaller adjustments on a daily basis, further compromising between daily and weekly planning.</li>
</ul>
<h3>What do PEPQs handle poorly?</h3>
<p>PEPQs don't do a very good job at representing work that doesn't break down into small, discrete tasks. Often I find I'll end up with things like &quot;figure out what I need to do for X,&quot; or &quot;spend some time doing Y.&quot; This mostly works, but these tasks are probably better suited for a different, parallel system.</p>
<h3>Implementing PEPQs with software</h3>
<p>To my knowledge, no applications support PEPQs both functionally and visually. You can recreate most of the functionality with a todo list app that supports assigning tasks due dates and priorities, and you can represent it pretty well visually with a weekly planner app, but few apps do both well in my opinion.</p>
<p>The way I do it is with a spread sheet. I typically create three spreadsheets a year: one for the winter, one for the summer, and one for the fall. Then, I create a new sheet for each week. The weekly sheet lists the days in the columns, and divides chunks of rows with different colours to represent the three priority queues. Then, as I complete tasks, style the cells with a strike through, which I've set up as a keyboard shortcut.</p>

      </main>

      <footer>
        <p class="dinkus" aria-hidden="true">* * *</p>
        

        <p>
          Unless otherwise noted, content is
          <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/" rel="license">
            CC-BY-NC-SA 4.0</a> license.<br/>All code is distributed under the
          <a href="https://www.gnu.org/licenses/agpl-3.0.en.html" rel="license">
          AGPL</a> license (<a href="https://codeberg.org/njms/pages">source</a>).
        </p>
        <p>Built with <a href="calathea.html">calathea</a></p>
      </footer>
    </div>
  </body>
</html>
